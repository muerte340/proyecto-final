# Proyecto fin de grado DAW 2020

por Davig Gonzalez Argueta

Sitio cursado: Cif Fp Batoi

Direccor de proyecto: Alexandre Coloma Gisbert


## Carga de proyecto


Despues de hacer el clone hay que ejecurar composer para cargar el vendor

```
composer update

```

En el .env hay que cambiar el entorno de trabajo y las credenciales de la BD al igual que el nombre


Los siguientes comandos permiten cargar la estructura de la base de datos

```
php bin/console doctrine:database:create <- te permite crear la base de datos
php bin/console doctrine:schema:create <- te permite crear la estructura de tablas

```

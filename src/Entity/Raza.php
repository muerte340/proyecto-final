<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Locale;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RazaRepository")
 */
class Raza
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pelaje", inversedBy="razas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pelaje;

    /**
     * @ORM\Column(type="smallint")
     */
    private $agresividad;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UsoComun", inversedBy="razas")
     */
    private $usosComunes;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $img;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LangRaza", mappedBy="raza",cascade={"All"})
     */
    private $languages;

    public function __construct()
    {
        $this->usosComunes = new ArrayCollection();
        $this->languages = new ArrayCollection();
    }

    public function __toString(){    
        return $this->getId()."";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPelaje(): ?Pelaje
    {
        return $this->pelaje;
    }

    public function setPelaje(?Pelaje $pelaje): self
    {
        $this->pelaje = $pelaje;

        return $this;
    }

    public function getAgresividad(): ?int
    {
        return $this->agresividad;
    }

    public function setAgresividad(int $agresividad): self
    {
        $this->agresividad = $agresividad;

        return $this;
    }

    /**
     * @return Collection|UsoComun[]
     */
    public function getUsosComunes(): Collection
    {
        return $this->usosComunes;
    }

    public function addUsosComune(UsoComun $usosComune): self
    {
        if (!$this->usosComunes->contains($usosComune)) {
            $this->usosComunes[] = $usosComune;
        }

        return $this;
    }

    public function removeUsosComune(UsoComun $usosComune): self
    {
        if ($this->usosComunes->contains($usosComune)) {
            $this->usosComunes->removeElement($usosComune);
        }

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return Collection|LangRaza[]
     */
    public function getLanguages(): Collection
    {
        return $this->languages;
    }
    /**
     * @return LangRaza
     */
    public function getLanguageByIsoCode($isoCode): LangRaza
    {
        $langFiltered = $this->languages->filter(fn($lang)=> $lang->getLang()->getIsoCode() == $isoCode); 
        if( $langFiltered->isEmpty())
            return null;
        return $langFiltered->first();
    }

    public function getLanguageByDefaultIsoCode(): LangRaza
    {
       return $this->getLanguageByIsoCode( Locale::getDefault());
    }

    public function addLanguage(LangRaza $language): self
    {
        if (!$this->languages->contains($language)) {
            $this->languages[] = $language;
            $language->setRaza($this);
        }

        return $this;
    }

    public function removeLanguage(LangRaza $language): self
    {
        if ($this->languages->contains($language)) {
            $this->languages->removeElement($language);
            // set the owning side to null (unless already changed)
            if ($language->getRaza() === $this) {
                $language->setRaza(null);
            }
        }

        return $this;
    }
    public function getDataForJson(): array{
        $langs = [];
        foreach($this->getLanguages() as $lang) {
            $langs [] = $lang-> getDataForJson();
        }
        $usosComunes = [];
        foreach($this->getUsosComunes() as $usoComun) {
            $usosComunes [] = $usoComun -> getDataForJson();
        }
        return  [
            'id'=> $this->getId(),
            'name'=> $this->getName(),
            'agresividad' => $this->getAgresividad(),
            'img' => $this->getImg(),
            'pelaje'=> $this->getPelaje()->getDataForJson(),
            'langs'=> $langs,
            'usos_comunes'=> $usosComunes
        ];

    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Locale;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UsoComunRepository")
 */
class UsoComun
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LangUsoComun", mappedBy="usoComun",cascade={"All"})
     */
    private $langs;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Raza", mappedBy="usosComunes")
     */
    private $razas;

    public function __construct()
    {
        $this->langs = new ArrayCollection();
        $this->razas = new ArrayCollection();
    }

    public function __toString(){    
        return $this->getId()."";
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId($id) {}

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|LangUsoComun[]
     */
    public function getLangs(): Collection
    {
        return $this->langs;
    }
    public function getLangByIsoCode($isoCode): LangUsoComun
    {
        $langFiltered = $this->langs->filter(fn($lang)=> $lang->getLang()->getIsoCode() == $isoCode); 
        if( $langFiltered->isEmpty())
            return null;
        return $langFiltered->first();
    }
    public function getLangByDefaultIsoCode(): LangUsoComun
    {
       return $this->getLangByIsoCode( Locale::getDefault());
    }

    public function addLang(LangUsoComun $lang): self
    {
        if (!$this->langs->contains($lang)) {
            $this->langs[] = $lang;
            $lang->setUsoComun($this);
        }

        return $this;
    }

    public function removeLang(LangUsoComun $lang): self
    {
        if ($this->langs->contains($lang)) {
            $this->langs->removeElement($lang);
            // set the owning side to null (unless already changed)
            if ($lang->getUsoComun() === $this) {
                $lang->setUsoComun(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Raza[]
     */
    public function getRazas(): Collection
    {
        return $this->razas;
    }

    public function addRaza(Raza $raza): self
    {
        if (!$this->razas->contains($raza)) {
            $this->razas[] = $raza;
            $raza->addUsosComune($this);
        }

        return $this;
    }

    public function removeRaza(Raza $raza): self
    {
        if ($this->razas->contains($raza)) {
            $this->razas->removeElement($raza);
            $raza->removeUsosComune($this);
        }

        return $this;
    }

    public function getDataForJson(): array{
        $langs = [];
        foreach($this->getLangs() as $lang) {
            $langs [] = $lang-> getDataForJson();
        }
        return  [
            'id'=> $this->getId(),
            'name'=> $this->getName(),
            'langs'=> $langs
        ];

    }

}

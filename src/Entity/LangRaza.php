<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LangRazaRepository")
 */
class LangRaza
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="razas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lang;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Raza", inversedBy="languages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $raza;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $resume;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $comportamiento;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $cuidados;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $personalidad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLang(): ?Language
    {
        return $this->lang;
    }

    public function setLang(?Language $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getRaza(): ?Raza
    {
        return $this->raza;
    }

    public function setRaza(?Raza $raza): self
    {
        $this->raza = $raza;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getComportamiento(): ?string
    {
        return $this->comportamiento;
    }

    public function setComportamiento(string $comportamiento): self
    {
        $this->comportamiento = $comportamiento;

        return $this;
    }

    public function getCuidados(): ?string
    {
        return $this->cuidados;
    }

    public function setCuidados(string $cuidados): self
    {
        $this->cuidados = $cuidados;

        return $this;
    }

    public function getPersonalidad(): ?string
    {
        return $this->personalidad;
    }

    public function setPersonalidad(string $personalidad): self
    {
        $this->personalidad = $personalidad;

        return $this;
    }

    public function getDataForJson(): array{
        return  [
            'id'=> $this->getId(),
            'iso_code'=> $this->getLang()->getIsoCode(),
            'name'=> $this->getName(),
            'description'=> $this->getDescription(),
            'resume'=> $this->getResume(),
            'personalidad'=>$this-> getPersonalidad(),
            'comportamiento'=>$this-> getComportamiento(),
            'cuidados'=> $this->getCuidados()
        ];

    }
}

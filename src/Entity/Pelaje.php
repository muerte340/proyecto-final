<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Locale;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PelajeRepository")
 */
class Pelaje
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LangPelaje", mappedBy="pelaje",cascade={"All"})
     */
    private $langs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Raza", mappedBy="pelaje")
     */
    private $razas;

    public function __construct()
    {
        $this->langs = new ArrayCollection();
        $this->razas = new ArrayCollection();
    }

    public function __toString(){    
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|LangPelaje[]
     */
    public function getLangs(): Collection
    {
        return $this->langs;
    }

    public function getLangByIsoCode($isoCode): LangPelaje
    {
        $langFiltered = $this->langs->filter(fn($lang)=> $lang->getLang()->getIsoCode() == $isoCode); 
        if( $langFiltered->isEmpty())
            return null;
        return $langFiltered->first();
    }

    public function getLanguageByDefaultIsoCode(): LangPelaje
    {
       return $this->getLangByIsoCode( Locale::getDefault());
    }

    public function addLang(LangPelaje $lang): self
    {
        if (!$this->langs->contains($lang)) {
            $this->langs[] = $lang;
            $lang->setPelaje($this);
        }

        return $this;
    }

    public function removeLang(LangPelaje $lang): self
    {
        if ($this->langs->contains($lang)) {
            $this->langs->removeElement($lang);
            // set the owning side to null (unless already changed)
            if ($lang->getPelaje() === $this) {
                $lang->setPelaje(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Raza[]
     */
    public function getRazas(): Collection
    {
        return $this->razas;
    }

    public function addRaza(Raza $raza): self
    {
        if (!$this->razas->contains($raza)) {
            $this->razas[] = $raza;
            $raza->setPelaje($this);
        }

        return $this;
    }

    public function removeRaza(Raza $raza): self
    {
        if ($this->razas->contains($raza)) {
            $this->razas->removeElement($raza);
            // set the owning side to null (unless already changed)
            if ($raza->getPelaje() === $this) {
                $raza->setPelaje(null);
            }
        }

        return $this;
    }

    public function getDataForJson(): array{
        $langs = [];
        foreach($this->getLangs() as $lang) {
            $langs [] = $lang-> getDataForJson();
        }
        return  [
            'id'=> $this->getId(),
            'name'=> $this->getName(),
            'langs'=> $langs
        ];

    }
}

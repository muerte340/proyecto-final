<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Locale;


/**
 * @ORM\Entity(repositoryClass="App\Repository\LanguageRepository")
 */
class Language
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $isoCode;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LangPelaje", mappedBy="lang")
     */
    private $pelajes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LangUsoComun", mappedBy="lang", cascade={"persist"})
     */
    private $usosComunes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LangRaza", mappedBy="lang")
     */
    private $razas;

    public function __construct()
    {
        $this->pelajes = new ArrayCollection();
        $this->usosComunes = new ArrayCollection();
        $this->razas = new ArrayCollection();
    }

    public function __toString(){    
     
        return $this->getIsoCode();
      }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsoCode(): ?string
    {
        return $this->isoCode;
    }

    public function setIsoCode(string $isoCode): self
    {
        $this->isoCode = $isoCode;

        return $this;
    }

    /**
     * @return Collection|LangPelaje[]
     */
    public function getPelajes(): Collection
    {
        return $this->pelajes;
    }

    public function addPelaje(LangPelaje $pelaje): self
    {
        if (!$this->pelajes->contains($pelaje)) {
            $this->pelajes[] = $pelaje;
            $pelaje->setLang($this);
        }

        return $this;
    }

    public function removePelaje(LangPelaje $pelaje): self
    {
        if ($this->pelajes->contains($pelaje)) {
            $this->pelajes->removeElement($pelaje);
            // set the owning side to null (unless already changed)
            if ($pelaje->getLang() === $this) {
                $pelaje->setLang(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LangUsoComun[]
     */
    public function getUsosComunes(): Collection
    {
        return $this->usosComunes;
    }

    public function addUsosComune(LangUsoComun $usosComune): self
    {
        if (!$this->usosComunes->contains($usosComune)) {
            $this->usosComunes[] = $usosComune;
            $usosComune->setLang($this);
        }

        return $this;
    }

    public function removeUsosComune(LangUsoComun $usosComune): self
    {
        if ($this->usosComunes->contains($usosComune)) {
            $this->usosComunes->removeElement($usosComune);
            // set the owning side to null (unless already changed)
            if ($usosComune->getLang() === $this) {
                $usosComune->setLang(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LangRaza[]
     */
    public function getRazas(): Collection
    {
        return $this->razas;
    }

    public function addRaza(LangRaza $raza): self
    {
        if (!$this->razas->contains($raza)) {
            $this->razas[] = $raza;
            $raza->setLang($this);
        }

        return $this;
    }

    public function removeRaza(LangRaza $raza): self
    {
        if ($this->razas->contains($raza)) {
            $this->razas->removeElement($raza);
            // set the owning side to null (unless already changed)
            if ($raza->getLang() === $this) {
                $raza->setLang(null);
            }
        }

        return $this;
    }

    public function getDataForJson(): array{
        return  [
            'id'=> $this->getId(),
            'iso_code'=> $this->getIsoCode(),
            'name'=> $this->getName()
        ];
    }
}

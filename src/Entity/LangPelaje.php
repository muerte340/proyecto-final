<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LangPelajeRepository")
 */
class LangPelaje
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="pelajes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lang;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pelaje", inversedBy="langs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pelaje;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $grosor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $largaria;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $cuidados;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLang(): ?Language
    {
        return $this->lang;
    }

    public function setLang(?Language $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getPelaje(): ?Pelaje
    {
        return $this->pelaje;
    }

    public function setPelaje(?Pelaje $pelaje): self
    {
        $this->pelaje = $pelaje;

        return $this;
    }

    public function getGrosor(): ?string
    {
        return $this->grosor;
    }

    public function setGrosor(string $grosor): self
    {
        $this->grosor = $grosor;

        return $this;
    }

    public function getLargaria(): ?string
    {
        return $this->largaria;
    }

    public function setLargaria(string $largaria): self
    {
        $this->largaria = $largaria;

        return $this;
    }

    public function getCuidados(): ?string
    {
        return $this->cuidados;
    }

    public function setCuidados(string $cuidados): self
    {
        $this->cuidados = $cuidados;

        return $this;
    }

    public function getDataForJson(): array{
        return  [
            'id'=> $this->getId(),
            'iso_code'=> $this->getLang()->getIsoCode(),
            'grosor'=> $this->getGrosor(),
            'largo'=>$this-> getLargaria(),
            'cuidados'=> $this->getCuidados()
        ];

    }
}

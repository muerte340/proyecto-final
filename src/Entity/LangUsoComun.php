<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LangUsoComunRepository")
 */
class LangUsoComun
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language", inversedBy="usosComunes",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $lang;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UsoComun", inversedBy="langs",cascade={"persist"})
     */
    private $usoComun;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $name;

    public function __toString(){    
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLang(): ?Language
    {
        return $this->lang;
    }

    public function setLang(?Language $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getUsoComun(): ?UsoComun
    {
        return $this->usoComun;
    }

    public function setUsoComun(?UsoComun $usoComun): self
    {
        $this->usoComun = $usoComun;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDataForJson(): array{
        return  [
            'id'=> $this->getId(),
            'iso_code'=> $this->getLang()->getIsoCode(),
            'name'=> $this->getName()
        ];

    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200429153540 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lang_pelaje (id INT AUTO_INCREMENT NOT NULL, lang_id INT NOT NULL, pelaje_id INT NOT NULL, grosor VARCHAR(255) NOT NULL, largaria VARCHAR(255) NOT NULL, cuidados VARCHAR(1000) NOT NULL, INDEX IDX_54A1BA3DB213FA4 (lang_id), INDEX IDX_54A1BA3D4765F735 (pelaje_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lang_raza (id INT AUTO_INCREMENT NOT NULL, lang_id INT NOT NULL, raza_id INT NOT NULL, name VARCHAR(300) NOT NULL, description VARCHAR(1000) NOT NULL, resume VARCHAR(300) NOT NULL, comportamiento VARCHAR(1000) NOT NULL, cuidados VARCHAR(1000) NOT NULL, personalidad VARCHAR(1000) NOT NULL, INDEX IDX_53629FC9B213FA4 (lang_id), INDEX IDX_53629FC98CCBB6A9 (raza_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, iso_code VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lang_uso_comun (id INT AUTO_INCREMENT NOT NULL, lang_id INT NOT NULL, uso_comun_id INT DEFAULT NULL, name VARCHAR(300) NOT NULL, INDEX IDX_493260E0B213FA4 (lang_id), INDEX IDX_493260E0C9C0A15E (uso_comun_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pelaje (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE raza (id INT AUTO_INCREMENT NOT NULL, pelaje_id INT NOT NULL, name VARCHAR(255) NOT NULL, agresividad SMALLINT NOT NULL, img VARCHAR(300) NOT NULL, INDEX IDX_4602D6AE4765F735 (pelaje_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE raza_uso_comun (raza_id INT NOT NULL, uso_comun_id INT NOT NULL, INDEX IDX_3B015CC8CCBB6A9 (raza_id), INDEX IDX_3B015CCC9C0A15E (uso_comun_id), PRIMARY KEY(raza_id, uso_comun_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE uso_comun (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lang_pelaje ADD CONSTRAINT FK_54A1BA3DB213FA4 FOREIGN KEY (lang_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE lang_pelaje ADD CONSTRAINT FK_54A1BA3D4765F735 FOREIGN KEY (pelaje_id) REFERENCES pelaje (id)');
        $this->addSql('ALTER TABLE lang_raza ADD CONSTRAINT FK_53629FC9B213FA4 FOREIGN KEY (lang_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE lang_raza ADD CONSTRAINT FK_53629FC98CCBB6A9 FOREIGN KEY (raza_id) REFERENCES raza (id)');
        $this->addSql('ALTER TABLE lang_uso_comun ADD CONSTRAINT FK_493260E0B213FA4 FOREIGN KEY (lang_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE lang_uso_comun ADD CONSTRAINT FK_493260E0C9C0A15E FOREIGN KEY (uso_comun_id) REFERENCES uso_comun (id)');
        $this->addSql('ALTER TABLE raza ADD CONSTRAINT FK_4602D6AE4765F735 FOREIGN KEY (pelaje_id) REFERENCES pelaje (id)');
        $this->addSql('ALTER TABLE raza_uso_comun ADD CONSTRAINT FK_3B015CC8CCBB6A9 FOREIGN KEY (raza_id) REFERENCES raza (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE raza_uso_comun ADD CONSTRAINT FK_3B015CCC9C0A15E FOREIGN KEY (uso_comun_id) REFERENCES uso_comun (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lang_pelaje DROP FOREIGN KEY FK_54A1BA3DB213FA4');
        $this->addSql('ALTER TABLE lang_raza DROP FOREIGN KEY FK_53629FC9B213FA4');
        $this->addSql('ALTER TABLE lang_uso_comun DROP FOREIGN KEY FK_493260E0B213FA4');
        $this->addSql('ALTER TABLE lang_pelaje DROP FOREIGN KEY FK_54A1BA3D4765F735');
        $this->addSql('ALTER TABLE raza DROP FOREIGN KEY FK_4602D6AE4765F735');
        $this->addSql('ALTER TABLE lang_raza DROP FOREIGN KEY FK_53629FC98CCBB6A9');
        $this->addSql('ALTER TABLE raza_uso_comun DROP FOREIGN KEY FK_3B015CC8CCBB6A9');
        $this->addSql('ALTER TABLE lang_uso_comun DROP FOREIGN KEY FK_493260E0C9C0A15E');
        $this->addSql('ALTER TABLE raza_uso_comun DROP FOREIGN KEY FK_3B015CCC9C0A15E');
        $this->addSql('DROP TABLE lang_pelaje');
        $this->addSql('DROP TABLE lang_raza');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE lang_uso_comun');
        $this->addSql('DROP TABLE pelaje');
        $this->addSql('DROP TABLE raza');
        $this->addSql('DROP TABLE raza_uso_comun');
        $this->addSql('DROP TABLE uso_comun');
    }
}

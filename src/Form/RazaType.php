<?php

namespace App\Form;

use App\Entity\Raza;
use App\Entity\Pelaje;
use App\Entity\UsoComun;
use App\Entity\LangRaza;
use App\Repository\PelajeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Doctrine\ORM\EntityManagerInterface;

class RazaType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $raza =$builder->getData();
        $idRaza = $raza == null? 0 : $raza->getId();
        $builder
            ->add('name')
            ->add('agresividad')
            ->add('img')
            ->add('pelaje', ChoiceType::class,[
                'choice_label' => 'getName',
                'label_format' => 'pelaje.singular.may',
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return $this->entityManager->getRepository(Pelaje::class)->findAll();
                }),
            ])
            ->add('languages',CollectionType::class, [
                'entry_type' =>  LangRazaType::class,
                'entry_options' => [
                    'label' => false,
                    'idRaza' =>$idRaza,
                ],
                'allow_add'=> true,
                'allow_delete'=> true,
                'prototype_name' => 'lang_raza',
                ])
            ->add('usosComunes',EntityType::class, [
                'class' => UsoComun::class,
                'choice_label' => 'getLangByDefaultIsoCode',
                'multiple' =>true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Raza::class,
            'label' => false
        ]);
    }
}

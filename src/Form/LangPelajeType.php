<?php

namespace App\Form;

use App\Entity\LangPelaje;
use App\Entity\Pelaje;
use App\Entity\Language;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Intl\Locale;
use Symfony\Component\Form\CallbackTransformer;

class LangPelajeType extends AbstractType
{

    private $entityManager;
    private $languageRepository;
    private $pelajepository;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->languageRepository = $this->entityManager->getRepository(Language::class);
        $this->pelajepository = $this->entityManager->getRepository(Pelaje::class);
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('grosor', null, [
                'required' => false,
                'row_attr' => ['class' => 'col-10 col-md-4'],
            ])
            ->add('largaria', null, [
                'required' => false,
                'row_attr' => ['class' => 'col-10 col-md-4'],
            ])
            ->add('cuidados', null, [
                'required' => false,
                'row_attr' => ['class' => 'col-10 col-md-4'],
            ])
            ->add('lang', LocaleType::class, [
                'required' => false,
                'label_format' => 'lang.singular.may',
                'row_attr' => ['class' => 'col-10 col-md-4'],
                'preferred_choices' =>[Locale::getDefault()],
            ])
            ->add('pelaje', HiddenType::class,['data'=>  $options['idPelaje']])
        ;
        $builder->get('lang')->addModelTransformer(new CallbackTransformer(
            function ($lang) {
                return $lang;
            },
            function ($stringLang) {
                if($stringLang != null){
                    return $this->languageRepository->getOrAdd($stringLang);
                }
            }
        ));
        $builder->get('pelaje')->addModelTransformer(new CallbackTransformer(
            function ($id) {
                return $id;
            },
            function ($id) {
                return $this->pelajepository->find((int)$id);
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LangPelaje::class,
            'empty_data'=> new LangPelaje(),
            'idPelaje' => 0,
            'required' => false,
            'attr' => ['class' => 'row mb-2 pb-2 justify-content-between border-bottom'],
        ]);
    }
}

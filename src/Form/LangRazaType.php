<?php

namespace App\Form;

use App\Entity\LangRaza;
use App\Entity\Raza;
use App\Entity\Language;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Intl\Locale;
use Symfony\Component\Form\CallbackTransformer;


class LangRazaType extends AbstractType
{
    private $entityManager;
    private $languageRepository;
    private $razapository;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->languageRepository = $this->entityManager->getRepository(Language::class);
        $this->razapository = $this->entityManager->getRepository(Raza::class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('raza',HiddenType::class,[
                'data'=>  $options['idRaza']])
            ->add('name', null,[
                'required' => false,
            'row_attr' => ['class' => 'col-10 col-md-4'],
            ])
            ->add('description', null,[
                'required' => false, 'empty_data' => '',
                'row_attr' => ['class' => 'col-10 col-md-4'],
            ])
            ->add('resume', null,[
                'required' => false,'empty_data' => '',
                'row_attr' => ['class' => 'col-10 col-md-4'],
            ])
            ->add('comportamiento', null,[
                'required' => false,'empty_data' => '',
                'row_attr' => ['class' => 'col-10 col-md-4'],
            ])
            ->add('cuidados', null,[
                'required' => false,'empty_data' => '',
                'row_attr' => ['class' => 'col-10 col-md-4'],
            ])
            ->add('personalidad', null, [
                'required' => false, 'empty_data' => '',
                'row_attr' => ['class' => 'col-10 col-md-4'],
            ])
            ->add('lang', LocaleType::class,[
                'required' => false,
                'label_format' => 'lang.singular.may',
                'row_attr' => ['class' => 'col-10 col-md-4'],
                'preferred_choices' =>[Locale::getDefault()],
            ]);
            $builder->get('lang')->addModelTransformer(new CallbackTransformer(
                function ($lang) {
                    return $lang;
                },
                function ($stringLang) {
                    if($stringLang != null){
                        return $this->languageRepository->getOrAdd($stringLang);
                    }
                }
            ));
            $builder->get('raza')->addModelTransformer(new CallbackTransformer(
                function ($id) {
                    return $id;
                },
                function ($id) {
                    return $this->razapository->find((int)$id);
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LangRaza::class,
            'empty_data'=> new LangRaza(),
            'idRaza' => 0,
            'required' => false,
            'attr' => ['class' => 'row mb-2 pb-2 justify-content-between border-bottom'],
        ]);
        $resolver->setAllowedTypes('idRaza', ['int','null']);

    }

}

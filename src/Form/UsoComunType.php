<?php

namespace App\Form;

use App\Entity\UsoComun;
use App\Entity\LangUsoComun;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityManagerInterface;

class UsoComunType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();
        $builder
            ->add('id',HiddenType::class)
            ->add('name')
            ->add('langs',CollectionType::class, [
                'entry_type' => LangUsoComunType::class,
                'entry_options' => [
                    'label' => false,
                    'idUsoComun'=> $entity->getId(),
                ],
                'attr' => ['class' => 'row my-2'],
                'allow_add'=> true,
                'allow_delete'=> true,
                'required'=>false,
                'prototype_name' => 'lang_uso_comun',
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UsoComun::class,
            'attr' => ['class' => 'mb-2 border-bottom'],
        ]);
    }
}

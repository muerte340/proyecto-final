<?php

namespace App\Form;

use App\Entity\Pelaje;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class PelajeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();
        $builder
            ->add('name')
            ->add('langs',CollectionType::class, [
                'entry_type' => LangPelajeType::class,
                'entry_options' => [
                    'label' => false,
                    'idPelaje'=> $entity->getId(),
                ],
                'allow_add'=> true,
                'allow_delete'=> true,
                'required'=>false,
                'prototype_name' => 'lang_pelaje',
                ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pelaje::class,
        ]);
    }
}

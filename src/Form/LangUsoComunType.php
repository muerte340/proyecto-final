<?php

namespace App\Form;

use App\Entity\LangUsoComun;
use App\Entity\Language;
use App\Entity\UsoComun;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Intl\Locale;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityManagerInterface;


class LangUsoComunType extends AbstractType
{
    private $entityManager;
    private $languageRepository;
    private $usoComunRepository;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->languageRepository = $this->entityManager->getRepository(Language::class);
        $this->usoComunRepository = $this->entityManager->getRepository(UsoComun::class);
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('lang',LocaleType::class,[
                'required' => false,
                'label_format' => 'lang.singular.may',
                'preferred_choices' =>[Locale::getDefault()],
            ])
            ->add('usoComun',HiddenType::class,[
                'data'=> $options['idUsoComun'],
                'required' => false,
            ])
            ->add('name', null,['required' => false,]);
        $builder->get('lang')->addModelTransformer(new CallbackTransformer(
            function ($lang) {
                return $lang;
            },
            function ($stringLang) {
                if($stringLang != null){
                    return $this->languageRepository->getOrAdd($stringLang);
                }
            }
        ));
        $builder->get('usoComun')->addModelTransformer(new CallbackTransformer(
            function ($id) {
                return $id;
            },
            function ($id) {
                return $this->usoComunRepository->find((int) $id);
            }
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LangUsoComun::class,
            'empty_data'=>function (FormInterface $form) {
                    return $form->isEmpty() && !$form->isRequired() ? null : new LangUsoComun();
            },
            'required' => false,
            'attr' => ['class' => 'col-10 col-md-4'],
            'idUsoComun' => 0,
        ]);
        $resolver->setAllowedTypes('idUsoComun', ['int','null']);
    }
}

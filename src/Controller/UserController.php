<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\RegisterType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\FormError;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="register", methods={"GET","POST"})
     */
     public function registro(TranslatorInterface $translator, Request $request,UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $user->setRoles([User::getAllRoles()['user']]);
            $user->setPassword($passwordEncoder->encodePassword($user, $form['password']->getData()));
            $userRepo = $entityManager->getRepository(User::class); // this needs to be the location of your user repository

            if ($userRepo->findOneBy(['email' => $user->getEmail()])) { // you need to pick a field that determines how you will search for the user via the repository
                $form->addError(new FormError($translator->trans("User already exists",[],"security")));
            }
            if($form->isValid()){
                $entityManager->persist($user);
                $entityManager->flush();

                return $this->redirectToRoute('inicio');
            }
        } 

        return $this->render('user/register.html.twig', [
            'user' => $user,
            'title_text'=> $translator->trans("Sign up"),
            'button_label'=>$translator->trans("Sign up"),
            'form' => $form->createView(),
        ]); 
    }

    /**
     * @Route("/admin/users/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/user/new", name="user_new", methods={"GET","POST"})
     */
    public function new(TranslatorInterface $translator,Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'title_text'=> $translator->trans("new.male.singular.may").' '.$translator->trans("user.singular.min"),
            'button_label'=> $translator->trans("create.may"),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/user/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/admin/user/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(TranslatorInterface $translator,Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'title_text'=> $translator->trans("edit.may").' '.$translator->trans("user.singular.min"),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/user/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}

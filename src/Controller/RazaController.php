<?php

namespace App\Controller;

use App\Entity\Raza;
use App\Entity\Pelaje;
use App\Form\RazaType;
use App\Repository\RazaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;


class RazaController extends AbstractController
{
    /**

     * @Route("/", name="inicio")
     */
    public function index(RazaRepository $razaRepository)
    {
        
        $razas = $razaRepository->findAll();
        $raza =$razaRepository->find(rand(1,count($razas)));
        return $this->render('raza/index.html.twig', [
            'destacado'=> $raza,
            'razas' => $razas,
        ]);
    }

    /**
     * @Route("/raza/{id}", name="raza" , requirements={"id"="\d+"})
     */
    public function getRaza($id)
    {

        $em = $this->getDoctrine()->getManager();
        $raza = $em->getRepository(Raza::class)->find($id);
        return $this->render('raza/raza.html.twig', [
            'raza' => $raza,
        ]);
    }

    // 

    /**
     * @Route("/admin/raza", name="raza_index", methods={"GET"})
     */
    public function adminIndex(RazaRepository $razaRepository): Response
    {
        return $this->render('raza/adminIndex.html.twig', [
            'razas' => $razaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/raza/new", name="raza_new", methods={"GET","POST"})
     */
    public function new(RazaRepository $razaRepository, Request $request): Response
    {
        $raza = new Raza();
        $form = $this->createForm(RazaType::class, $raza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach($raza->getLanguages() as $lang){
                $lang->setRaza($raza);
            }
            $razaRepository->persist($raza);
            return $this->redirectToRoute('raza_index');
        }

        return $this->render('raza/new.html.twig', [
            'raza' => $raza,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/raza/{id}", name="raza_show", methods={"GET"})
     */
    public function show(Raza $raza): Response
    {
        return $this->render('raza/show.html.twig', [
            'raza' => $raza,
        ]);
    }

    /**
     * @Route("/admin/raza/{id}/edit", name="raza_edit", methods={"GET","POST"})
     */
    public function edit(RazaRepository $razaRepository, Request $request, Raza $raza): Response
    {
        $form = $this->createForm(RazaType::class, $raza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $raza = $form->getData();
            $langsRemoved = [];
            foreach ($raza->getLanguages() as  $lang) {
                if($lang->getName() == null){
                    $langsRemoved[]=$lang;
                }
            }
            foreach ($langsRemoved as  $lang) {
                $raza->removeLanguage($lang);
            }
            $razaRepository->updateRaza($raza);

            return $this->redirectToRoute('raza_index');
        }

        return $this->render('raza/edit.html.twig', [
            'raza' => $raza,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/raza/{id}", name="raza_delete", methods={"DELETE"})
     */
    public function delete(RazaRepository $razaRepository, Request $request, Raza $raza): Response
    {
        if ($this->isCsrfTokenValid('delete'.$raza->getId(), $request->request->get('_token'))) {
            $razaRepository->removeRaza($raza);
        }

        return $this->redirectToRoute('raza_index');
    }

    /**
     * @Route("/api/raza/{id}", name="api_raza_show", methods={"GET"})
     */
    public function apiGetOne(Raza $raza): JsonResponse
    {
        return new JsonResponse( $raza->getDataForJson(), Response::HTTP_OK);
    }
     /**
     * @Route("/api/raza/", name="api_raza_index", methods={"GET"})
     */
    public function apiGetAll(RazaRepository $razaRepository): JsonResponse
    {
        $razas =$razaRepository->findAll();
        $data =[];

        foreach ($razas as $raza) {
            $data[] = $raza->getDataForJson();
        }

        return new JsonResponse(["razas" => $data], Response::HTTP_OK);
    }

}

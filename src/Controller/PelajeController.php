<?php

namespace App\Controller;

use App\Entity\Pelaje;
use App\Form\PelajeType;
use App\Repository\PelajeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;


class PelajeController extends AbstractController
{
    /**
     * @Route("/admin/pelaje", name="pelaje_index", methods={"GET"})
     */
    public function index(PelajeRepository $pelajeRepository): Response
    {
        return $this->render('pelaje/index.html.twig', [
            'pelajes' => $pelajeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/pelaje/new", name="pelaje_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pelaje = new Pelaje();
        $form = $this->createForm(PelajeType::class, $pelaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach($pelaje->getLangs() as $lang){
                $lang->setPelaje($pelaje);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pelaje);
            $entityManager->flush();

            return $this->redirectToRoute('pelaje_index');
        }

        return $this->render('pelaje/new.html.twig', [
            'pelaje' => $pelaje,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/pelaje/{id}", name="pelaje_show", methods={"GET"})
     */
    public function show(Pelaje $pelaje): Response
    {
        return $this->render('pelaje/show.html.twig', [
            'pelaje' => $pelaje,
        ]);
    }

    /**
     * @Route("/admin/pelaje/{id}/edit", name="pelaje_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pelaje $pelaje): Response
    {
        $form = $this->createForm(PelajeType::class, $pelaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pelaje = $form->getData();
            $langsRemoved = [];
            foreach ($pelaje->getLangs() as  $lang) {
                if($lang->getGrosor() == null){
                    $langsRemoved[]=$lang;
                }
            }
            foreach ($langsRemoved as  $lang) {
                $pelaje->removeLang($lang);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pelaje_index');
        }

        return $this->render('pelaje/edit.html.twig', [
            'pelaje' => $pelaje,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/pelaje/{id}", name="pelaje_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pelaje $pelaje): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pelaje->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pelaje);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pelaje_index');
    }

    /**
     * @Route("/api/pelaje/{id}", name="api_pelaje_show", methods={"GET"})
     */
    public function apiGetOne(Pelaje $pelaje): JsonResponse
    {
        return new JsonResponse($pelaje->getDataForJson(), Response::HTTP_OK);
    }
     /**
     * @Route("/api/pelaje/", name="api_pelaje_index", methods={"GET"})
     */
    public function apiGetAll(PelajeRepository $pelajeRepository): JsonResponse
    {
        $pelajes =$pelajeRepository->findAll();
        $data =[];

        foreach ($pelajes as $pelaje) {
            $data[] = $pelaje->getDataForJson();
        }

        return new JsonResponse(['pelajes' => $data], Response::HTTP_OK);
    }
}

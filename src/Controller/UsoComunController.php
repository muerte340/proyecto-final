<?php

namespace App\Controller;

use App\Entity\UsoComun;
use App\Form\UsoComunType;
use App\Repository\UsoComunRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class UsoComunController extends AbstractController
{
    /**
     * @Route("/admin/uso-comun", name="uso_comun_index", methods={"GET"})
     */
    public function index(UsoComunRepository $usoComunRepository): Response
    {
        return $this->render('uso_comun/index.html.twig', [
            'uso_comuns' => $usoComunRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/uso-comun/new", name="uso_comun_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $usoComun = new UsoComun();
        $form = $this->createForm(UsoComunType::class, $usoComun);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach($usoComun->getLangs() as $lang){
                $lang->setUsoComun($usoComun);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($usoComun);
            $entityManager->flush();

            return $this->redirectToRoute('uso_comun_index');
        }

        return $this->render('uso_comun/new.html.twig', [
            'uso_comun' => $usoComun,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/uso-comun/{id}", name="uso_comun_show", methods={"GET"})
     */
    public function show(UsoComun $usoComun): Response
    {
        return $this->render('uso_comun/show.html.twig', [
            'uso_comun' => $usoComun,
        ]);
    }

    /**
     * @Route("/admin/uso-comun/{id}/edit", name="uso_comun_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UsoComun $usoComun): Response
    {
        $form = $this->createForm(UsoComunType::class, $usoComun);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usoComun = $form->getData();
            $langsRemoved = [];
            foreach ($usoComun->getLangs() as  $lang) {
                if($lang->getName() == null){
                    $langsRemoved[]=$lang;
                }
            }
            foreach ($langsRemoved as  $lang) {
                $usoComun->removeLang($lang);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('uso_comun_index');
        }

        return $this->render('uso_comun/edit.html.twig', [
            'uso_comun' => $usoComun,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/uso-comun/{id}", name="uso_comun_delete", methods={"DELETE"})
     */
    public function delete(Request $request, UsoComun $usoComun): Response
    {
        if ($this->isCsrfTokenValid('delete'.$usoComun->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($usoComun);
            $entityManager->flush();
        }

        return $this->redirectToRoute('uso_comun_index');
    }
     /**
     * @Route("/api/uso-comun/{id}", name="api_uso_comun_show", methods={"GET"})
     */
    public function apiGetOne(UsoComun $usoComun): JsonResponse
    {
        return new JsonResponse( $usoComun->getDataForJson(), Response::HTTP_OK);
    }
     /**
     * @Route("/api/uso-comun/", name="api_uso_comun_index", methods={"GET"})
     */
    public function apiGetAll(UsoComunRepository $usosComunespository): JsonResponse
    {
        $usosComunes =$usosComunesRepository->findAll();
        $data =[];

        foreach ($razas as $usoComun) {
            $data[] = $usoComun->getDataForJson();
        }

        return new JsonResponse(["usos-comunes" => $data], Response::HTTP_OK);
    }
}

<?php

namespace App\Repository;

use App\Entity\LangUsoComun;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LangUsoComun|null find($id, $lockMode = null, $lockVersion = null)
 * @method LangUsoComun|null findOneBy(array $criteria, array $orderBy = null)
 * @method LangUsoComun[]    findAll()
 * @method LangUsoComun[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LangUsoComunRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LangUsoComun::class);
    }

    // /**
    //  * @return LangUsoComun[] Returns an array of LangUsoComun objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LangUsoComun
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

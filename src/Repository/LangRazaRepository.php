<?php

namespace App\Repository;

use App\Entity\LangRaza;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LangRaza|null find($id, $lockMode = null, $lockVersion = null)
 * @method LangRaza|null findOneBy(array $criteria, array $orderBy = null)
 * @method LangRaza[]    findAll()
 * @method LangRaza[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LangRazaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LangRaza::class);
    }

    // /**
    //  * @return LangRaza[] Returns an array of LangRaza objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LangRaza
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

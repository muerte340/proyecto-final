<?php

namespace App\Repository;

use App\Entity\Raza;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Raza|null find($id, $lockMode = null, $lockVersion = null)
 * @method Raza|null findOneBy(array $criteria, array $orderBy = null)
 * @method Raza[]    findAll()
 * @method Raza[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RazaRepository extends ServiceEntityRepository
{
    private $manager;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Raza::class);
        $this->manager = $this->getEntityManager();
    }

    public function saveRaza($name, $img, $pelaje, $agresividad, $usosComunos)
    {
        $newRaza = new Raza();

        $newRaza
            ->setName($name)
            ->setImg($img)
            ->setPelaje($pelaje)
            ->setAgresividad($agresividad)
            ->setUsosComunes($usosComunos);

        $this->manager->persist($newRaza);
        $this->manager->flush();
    }
    public function persist(Raza $newRaza)
    {
        $this->manager->persist($newRaza);
        $this->manager->flush();
    }

    public function updateRaza(Raza $raza): Raza
    {
        $this->manager->persist($raza);
        $this->manager->flush();

        return $raza;
    }


    public function removeRaza(Raza $raza)
    {
        $this->manager->remove($raza);
        $this->manager->flush();
    }

    // /**
    //  * @return Raza[] Returns an array of Raza objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Raza
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

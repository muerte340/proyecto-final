<?php

namespace App\Repository;

use App\Entity\UsoComun;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UsoComun|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsoComun|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsoComun[]    findAll()
 * @method UsoComun[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsoComunRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsoComun::class);
    }

    // /**
    //  * @return UsoComun[] Returns an array of UsoComun objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsoComun
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

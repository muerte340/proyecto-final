<?php

namespace App\Repository;

use App\Entity\Pelaje;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pelaje|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pelaje|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pelaje[]    findAll()
 * @method Pelaje[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PelajeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pelaje::class);
    }

    // /**
    //  * @return Pelaje[] Returns an array of Pelaje objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pelaje
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
